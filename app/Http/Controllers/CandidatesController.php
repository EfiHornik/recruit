<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;

// full name is "App\Http\Controllers\CandidateController"
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
           {        
            $candidates = Candidate::all();
            return view('candidates.index', compact('candidates'));
             }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                //
                return view('candidates.create');
             }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
             
    {
        $candidate=new Candidate();
        // $candidate->name= $request->name;   //////request all better then this 2 lines (mass assingment)
        // $candidate->email= $request->email;
        $candidate->save($request->all());
        return redirect('candidates');


        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidates=Candidate::findOrFail($id);
        return view('candidates.edit',compact('candidates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->update($request->all());
        $candidate->save();

        return redirect('candidates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return rediect('candidates');

    }



}